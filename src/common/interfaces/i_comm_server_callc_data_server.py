from abc import ABC, abstractmethod
from typing import List
import uuid

from common.data_structures.user import User
from common.data_structures.message import Message
from common.data_structures.move import Move
from common.data_structures.local_game import LocalGame
from common.data_structures.public_game import PublicGame
from common.data_structures.profiles import Profile, Player


class I_CommServerCallsDataServer(ABC):
    @abstractmethod
    def create_game(self, game: LocalGame) -> PublicGame:
        pass

    @abstractmethod
    def add_spectator_to_game(
        self, user_id: uuid.UUID, game_id: uuid.UUID
    ) -> LocalGame:
        pass

    @abstractmethod
    def add_message_to_game(self, message: Message, game_id: uuid.UUID) -> LocalGame:
        pass

    @abstractmethod
    def add_player_to_game(self, user_id: uuid.UUID, game_id: uuid.UUID) -> LocalGame:
        pass

    @abstractmethod
    def add_move_to_game(self, move: Move, game_id: uuid.UUID) -> None:
        pass

    @abstractmethod
    def add_user(self, profile: Profile) -> Player:
        pass

    @abstractmethod
    def get_available_games(self) -> List[PublicGame]:
        pass

    @abstractmethod
    def get_game_spectators(self, game_id: uuid.UUID) -> List[User]:
        pass

    @abstractmethod
    def get_game_players(self, game_id: uuid.UUID) -> List[User]:
        pass

    @abstractmethod
    def get_profile(self, user_id: uuid.UUID) -> Profile:
        pass

    @abstractmethod
    def get_players(self) -> List[Player]:
        pass

    @abstractmethod
    def is_game_finished(self, game_id: uuid.UUID) -> bool:
        pass

    @abstractmethod
    def apply_result_to_profile(self, geme_id: uuid.UUID) -> None:
        pass

    @abstractmethod
    def update_profile(self, profile: Profile) -> None:
        pass

    @abstractmethod
    def delete_spectator_from_game(
        self, user_id: uuid.UUID, game_id: uuid.UUID
    ) -> None:
        pass

    @abstractmethod
    def delete_user(self, user_id: uuid.UUID) -> None:
        pass

    @abstractmethod
    def delete_game(self, game_id: uuid.UUID) -> None:
        pass
