from common.data_structures.data_server import DataServer


class I_CommServerCallsDataServer(ABC):
    @abstractmethod
    def start_server(self) -> DataServer:
        pass

    @abstractmethod
    def quit_server(self):
        pass
