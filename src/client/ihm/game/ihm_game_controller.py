import typing

from client.ihm.game.views.game_view import GameView
from common.interfaces.i_ihm_game_calls_comm import I_IHMGameCallsComm
from common.interfaces.i_ihm_main_calls_ihm_game import I_IHMMainCallsIHMGame
from common.interfaces.i_ihm_game_calls_ihm_main import I_IHMGameCallsIHMMain
from common.interfaces.i_comm_calls_ihm_game import I_CommCallsIHMGame


class IHMGameController:
    def __init__(
        self,
        pygame_controller: typing.Any,  # no type because of circular import
        my_interface_from_ihm_main: I_IHMMainCallsIHMGame = None,
        my_interface_from_comm: I_CommCallsIHMGame = None,
        my_interface_to_comm: I_IHMGameCallsComm = None,
        my_interface_to_ihm_main: I_IHMGameCallsIHMMain = None,
    ):
        self.pygame_controller = pygame_controller
        self.my_interface_from_ihm_main = my_interface_from_ihm_main
        self.my_interface_from_comm = my_interface_from_comm
        self.my_interface_to_comm = my_interface_to_comm
        self.my_interface_to_ihm_main = my_interface_to_ihm_main
        # self.my_interface_to_data = my_interface_to_data

        # initialize the first view
        self.game_view = GameView(
            pygame_controller.get_ui_manager(),
            pygame_controller.get_ui_renderer(),
            self,
        )
        pygame_controller.show_view(self.game_view)

    def set_my_interface_to_comm(self, i: I_IHMGameCallsComm) -> None:
        self.my_interface_to_comm = i

    def set_my_interface_to_ihm_main(self, i: I_IHMGameCallsIHMMain) -> None:
        self.my_interface_to_ihm_main = i

    #    def set_my_interface_to_data(self, i: I_IHMMainCallsData) -> None:
    #        self.my_interface_to_data = i

    def get_my_interface_from_ihm_main(self) -> I_IHMMainCallsIHMGame:
        if self.my_interface_from_ihm_main is not None:
            return self.my_interface_from_ihm_main
        else:
            raise Exception(
                "get_my_interface_from_ihm_main was called but my_interface_from_ihm_main is None"
            )

    def get_my_interface_from_comm(self) -> I_CommCallsIHMGame:
        if self.my_interface_from_comm is not None:
            return self.my_interface_from_comm
        else:
            raise Exception(
                "get_my_interface_from_comm was called but my_interface_from_comm is None"
            )
